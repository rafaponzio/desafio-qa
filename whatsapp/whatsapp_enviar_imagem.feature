#language: pt

Funcionalidade: Enviar uma Imagem no WhatsApp para IOS

Contexto:
    Dado que eu abra o aplicativo WhatsApp
    E eu esteja logado

Cenário: Enviar uma imagem em um grupo existente
    Dado que eu estou na página de conversas
    E toco na conversa em grupo "QAs"
    E toco no ícone "+"
    E toco na opção "Fotos e Vídeos"
    E toco na foto que desejo enviar
    Quando toco no ícone "Enviar"
    Então a mensagem deve ser enviada

Cenário: Encaminhar imagem de um grupo
    Dado que eu estou na página de conversas
    E toco na conversa em grupo "QAs"
    E toco no icone "Compartilhar" em frente a última imagem recebida
    E seleciono o contato "Rafael"
    E toco no botão "Encaminhar"
    Então a imagem é enviada ao contato
