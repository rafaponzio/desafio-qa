#language: pt

Funcionalidade: Criar um grupo no WhatsApp para IOS

Contexto:
    Dado que eu abra o aplicativo WhatsApp
    E eu esteja logado

Cenário: Criar um grupo no WhatsApp
    Dado que eu estou na página de conversas
    E toco no botão "Novo Grupo"
    E seleciono os 10 primeiros contatos
    E toco no botão "Seguinte"
    E preencho o nome do grupo como "Teste"
    Quando toco no botão "Criar"
    Então o grupo com o nome "Teste" e os 10 participantes é criado