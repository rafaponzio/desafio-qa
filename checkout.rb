class CheckOut
    def initialize(pricing, discount)
        @pricing = pricing
        @discount = discount
        @cart = Hash.new
    end

    def scan(item)
        @cart[item] ||= 0
        @cart[item] += 1  
    end

    def total
        calculate_discount
    end


    def get_price(item)
        @pricing[item]
    end

    def get_discount(item)        
        min_qty = @discount[item].split(", ")[0].to_i
        discount = @discount[item].split(", ")[1].to_i
        return 0 if @cart[item] < min_qty || min_qty.zero?
            discount * (@cart[item] / min_qty)
    end

    def calculate_discount
        sum = 0
        @cart.each do |item, qty|
            sum += (get_price(item) * qty)
            sum -= get_discount(item)
        end
        sum
    end

end

PRICING = {
    'A' => 50,
    'B' => 30,
    'C' => 20,
    'D' => 15
}

DISCOUNT = {
    'A' => '3, 20',
    'B' => '2, 15',
    'C' => '',
    'D' => ''
}

